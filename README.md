# realwds's website

> 2014 Anhui University of science and technology graduate, <br />
> major in information and computing science, is a kicker of mathematics department.<br />
> Now living in Hangzhou, Zhejiang Province, front-end typist, <br />
> The most commonly used buttons ```CTRL+C``` ```CTRL+V```。

### My Project

- [**sina news**](https://realwds.github.io/sina-news/)
- [**sina gif**](https://realwds.github.io/sina-gif/)
- [**vuepress theme blog**](https://realwds.github.io/vuepress-blog/)
- [**hexo theme blog**](https://realwds.github.io/hexo-blog/)
- [**gulp person website**](https://realwds.github.io/gulp-person-website/)
